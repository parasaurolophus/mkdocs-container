Copyright &copy; Kirk Rader 2020

# parasaurolophus/mkdocs

Minimalist docker image for mkdocs with mermaid support.

To use mermaid in your mkdocs project, add the following to `mkdocs.yml`

```yaml
plugins:
  - mermaid2

extra_javascript:
    - https://unpkg.com/mermaid@8.5.2/dist/mermaid.min.js
```

To run the image in your project directory:

```bash
docker run --rm -it \
  -w $(pwd) \
  -v $(pwd):$(pwd) \
  -u $(id -u):$(id -g) \
  parasaurolophus/mkdocs \
  mkdocs build --clean
```

See:

- <https://www.mkdocs.org/>
- <https://github.com/fralau/mkdocs-mermaid2-plugin>
