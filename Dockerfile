FROM alpine

#RUN apk add --no-cache bash gcc python3 py3-pip
#RUN pip3 install mkdocs mkdocs-mermaid-plugin

RUN apk add --no-cache \
    bash \
    python3 \
    python3-dev \
    py3-pip \
    build-base && \
  pip3 install mkdocs mkdocs-mermaid2-plugin
